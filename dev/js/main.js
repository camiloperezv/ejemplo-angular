(function() {
  var root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  root.AngularController = (function() {
    var appPrincipal;

    function AngularController() {}

    appPrincipal = angular.module('AngularApp', []);

    appPrincipal.controller('ControladorEstudiantes', function($scope, $http) {
      $http.get('../data/package.json').success(function(response) {
        console.log(response);
        return $scope.estudiantes = response;
      });
      return $scope.crearEstudiante = function() {
        var cedula, correo, nombre, notas;
        nombre = $("#nombre").val();
        cedula = $("#cedula").val();
        correo = $("#correo").val();
        notas = [0, 0, 0, 0];
        $scope.estudiantes[cedula] = {
          'nombre': nombre,
          'id': cedula,
          'correo': correo,
          'notas': notas
        };
        nombre = $("#nombre").val("");
        cedula = $("#cedula").val("");
        correo = $("#correo").val("");
        $("#myModal").modal('hide');
        return false;
      };
    });

    return AngularController;

  })();

}).call(this);
