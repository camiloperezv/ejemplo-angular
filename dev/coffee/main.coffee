root = exports ? this

class root.AngularController
  appPrincipal = angular.module('AngularApp', [])
  appPrincipal.controller 'ControladorEstudiantes', ($scope,$http) ->
    $http.get('../data/package.json').success (response) ->
      console.log response
      $scope.estudiantes = response
    $scope.crearEstudiante = ()->
      nombre = $("#nombre").val()
      cedula = $("#cedula").val()
      correo = $("#correo").val()
      notas = [0,0,0,0]
      $scope.estudiantes[cedula] = {
        'nombre':nombre
        'id':cedula
        'correo':correo
        'notas':notas
      }
      nombre = $("#nombre").val("")
      cedula = $("#cedula").val("")
      correo = $("#correo").val("")
      $("#myModal").modal 'hide'
      return false